import * as packageList from '../package_list/package_list.js';
import * as packageService from '../add_package/package_service.js';

const editTitle = document.getElementById('fEditPkgTitle');
const editCity = document.getElementById('fEditPkgCity');
const editSpots = document.getElementById('fEditPkgSpots');
const editPkg = document.getElementById('fEditPackage');
const titleError = document.getElementById('fEditTitleError');
const cityError = document.getElementById('fEditCityError');
const spotsError = document.getElementById('fEditSpotsError');
const editPkgModal = document.getElementById('editPackageModal');
const closeEdit = document.getElementById('closeEditPackage');

function setValidator(element, f) {
    element.addEventListener('focusout', f);
}

function setTitleValidator() {
    setValidator(editTitle, () => {
        const re = /^[a-zA-Z\s]+$/;
        if (re.exec(editTitle.value) == null) {
            editTitle.style.backgroundColor = "rgba(255,0,0,0.5)";
            titleError.innerText = "The Package title should only include letters and spaces";
        } else {
            editTitle.style.backgroundColor = 'white';
            titleError.innerText = "";
        }
    });
}

function setCityValidator() {
    setValidator(editCity, () => {
        const re = /^[a-zA-Z]+$/;
        if (re.exec(editCity.value) == null) {
            editCity.style.backgroundColor = "rgba(255,0,0,0.5)";
            cityError.innerText = "The Package city should only include letters";
        } else {
            editCity.style.backgroundColor = 'white';
            cityError.innerText = "";
        }
    });
}

function setSpotsValidator() {
    setValidator(editSpots, () => {
        const re = /^\d+$/;
        if (re.exec(editSpots.value) == null) {
            editSpots.style.backgroundColor = "rgba(255,0,0,0.5)";
            spotsError.innerText = "The Package spots should be a number";
        } else {
            editSpots.style.backgroundColor = 'white';
            spotsError.innerText = "";
        }
    });
}

function setEditPkgCallback() {
    editPkg.addEventListener('click', () => {
        if (editTitle.value != '' && titleError.innerText == ''
        && editCity.value != ''&& cityError.innerText == ''
        && editSpots.value != '' && spotsError.innerText == '') {
            packageService.editPackage(
                editTitle.value,
                editCity.value,
                parseInt(editSpots.value, 10)
            );
            editPkgModal.style.display = 'none';
            packageList.render();
        }
        else {
            alert("Please fill all form fields and consider the error messages.");
        }
    });
}

function setup() {
    const pkg = packageService.getActivePkg();
    editTitle.value = pkg.title;
    editCity.value = pkg.city;
    editSpots.value = pkg.spotsAvailable;
    editTitle.style.backgroundColor = 'white';
    titleError.innerText = "";
    editCity.style.backgroundColor = 'white';
    cityError.innerText = "";
    editSpots.style.backgroundColor = 'white';
    spotsError.innerText = "";
    setTitleValidator();
    setCityValidator();
    setSpotsValidator();
    setEditPkgCallback();
    editPkgModal.style.display = 'block';
}

closeEdit.addEventListener('click', () => {
    // Here we close the modal
    editPkgModal.style.display = 'none';
})

export {
    setup,
}