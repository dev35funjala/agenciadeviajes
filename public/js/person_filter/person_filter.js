import * as personList from "../person_list/person_list.js";

const applyFilterBtn = document.getElementById('personApplyFilter');
const filterOption = document.getElementById('personFilterOption');

export function setup() {
    setupInput();
    setupBtn();
}

function setupInput() {
    filterOption.addEventListener('change', () => setupFilter(filterOption.value));
}

function setupBtn() {
    applyFilterBtn.addEventListener("click", () => {
        personList.render();
    });
}

function setupFilter(option) {
    const filterValue = document.getElementById('personFilterValue');
    if (option != 'None') {
        applyFilterBtn.disabled = false;
    } else {
        applyFilterBtn.disabled = true;
    }
    const input = document.createElement('input');
    input.id = "personFilterValue";
    switch (option) {
        case "None":
            input.type = "text";
            input.disabled = true;
            personList.render();
            break;
        default:
            input.type = "text";
            break;
    }
    filterValue.parentNode.replaceChild(input, filterValue);
}

export function setToNone() {
    filterOption.value = 'None';
    setupFilter('None');
}