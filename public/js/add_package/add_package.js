import * as packageService from "./package_service.js"
import { Package } from "../Models/package.js";
import * as packageList from "../package_list/package_list.js"

const packageTitle = document.getElementById('fPackageTitle');
const packageCity = document.getElementById('fpackageCity');
const packageSpotsAvailable = document.getElementById('fpackageSpotsAvailable');
const packageAddPackage = document.getElementById('fAddPackage');
const titleError = document.getElementById('fTitlePackageError');
const cityError = document.getElementById('fCityPackageError');
const spotsError = document.getElementById('fSpotsPackageError');

function setValidator(element, f) {
    element.addEventListener('focusout', f);
}

function setTitleValidator() {
    setValidator(packageTitle, () => {
        const re = /^[a-zA-Z\s]+$/;
        if (re.exec(packageTitle.value) == null) {
            packageTitle.style.backgroundColor = "rgba(255,0,0,0.5)";
            titleError.innerText = "The Package title should only include letters and spaces";
        } else {
            packageTitle.style.backgroundColor = 'white';
            titleError.innerText = "";
        }
    });
}

function setCityValidator() {
    setValidator(packageCity, () => {
        const re = /^[a-zA-Z\s,]+$/;
        if (re.exec(packageCity.value) == null) {
            packageCity.style.backgroundColor = "rgba(255,0,0,0.5)";
            cityError.innerText = "The Package city should only include letters and spaces";
        } else {
            packageCity.style.backgroundColor = 'white';
            cityError.innerText = "";
        }
    });
}

function setSpotsValidator() {
    setValidator(packageSpotsAvailable, () => {
        const re = /^\d+$/;
        if (re.exec(packageSpotsAvailable.value) == null) {
            packageSpotsAvailable.style.backgroundColor = "rgba(255,0,0,0.5)";
            spotsError.innerText = "The Package spots should be a number";
        } else {
            packageSpotsAvailable.style.backgroundColor = 'white';
            spotsError.innerText = "";
        }
    });
}

function setAddPackageCallback() {
    packageAddPackage.addEventListener('click', () => {
        if (packageTitle.value != '' && titleError.innerText == ''
        && packageCity.value != ''&& cityError.innerText == ''
        && packageSpotsAvailable.value != '' && spotsError.innerText == '') {
                packageService.addPackage( new Package(
                    packageService.getPackageId(),
                    packageTitle.value,
                    packageCity.value,
                    parseInt(packageSpotsAvailable.value, 10)
                ));
                packageTitle.value = '';
                packageCity.value = '';
                packageSpotsAvailable.value = '';
            }
        else {
            alert("Please fill all form fields and consider the error messages.");
        }
        packageList.render();
    });
}

export {
    setTitleValidator,
    setCityValidator,
    setSpotsValidator,
    setAddPackageCallback,
}