export class Package {
    constructor(id, title, city, spotsAvailable, options) {
        this.id = id;
        this.title = title;
        this.city = city;
        this.spotsAvailable = spotsAvailable;
        if (options == null) {
            this.activities = [];
            this.registered = []; // people who buyed the package
            this.reservations = []; // people who reserved the package
        } else {
            this.activities = options.activities;
            this.registered = options.registered; // people who buyed the package
            this.reservations = options.reservations; // people who reserved the package
        }
    }

    get people() {
        return [...this.registered,...this.reservations];
    }

    get price() {
        switch (this.activities.length){
            case 0:
                return 0;
            case 1:
                return parseFloat(this.activities[0].price);
        }
        const activityPrices = this.activities.map((a) => parseFloat(a.price));
        return activityPrices.reduce((a, b) => a + b, 0.0).toFixed(2);
    }

    get profit() {
        return (this.price * this.people.length).toFixed(2);
    }

    get startDate() {
        const dates = this.activities.map((a) => Date.parse(a.date)).sort((a, b) => b - a);
        return dates[0];
    }
    
    hasSpotsAvailable() {
        return this.people.length < this.spotsAvailable;
    }
}