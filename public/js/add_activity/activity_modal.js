import * as activity from "./add_activity.js"

const newActivityBtn = document.getElementById('newActivityBtn');
const newActivityModal = document.getElementById('newActivityModal');
const closeActivityModal = document.getElementById('closeActivityModal');
const title = document.getElementById("fActivityTitle");
const type = document.getElementById("fActivityType");
const price = document.getElementById("fActivityPrice");
const initialTime = document.getElementById("fActivityInitialTime");
const finishTime = document.getElementById("fActivityFinishTime");
const place = document.getElementById("fActivityPlace");
const titleError = document.getElementById("fTitleError");
const typeError = document.getElementById("fTypeError");
const priceError = document.getElementById("fPriceError");
const initialTimeError = document.getElementById("fInitialTimeError");
const finishTimeError = document.getElementById("fFinishTimeError");
const placeError = document.getElementById("fPlaceError");

export function activityModalCfg(){
    activity.setTitleValidator();
    activity.setTypeValidator();
    activity.setPriceValidator();
    activity.setInitialTimeValidator();
    activity.setFinishTimeValidator();
    activity.setPlaceValidator();
    activity.setButtonListener();

    newActivityBtn.addEventListener('click', () => {
        // Here we open the modal
        title.style.backgroundColor = "white";
        titleError.innerText = "";
        type.style.backgroundColor = "white";
        typeError.innerText = "";
        price.style.backgroundColor = "white";
        priceError.innerText = "";
        initialTime.style.backgroundColor = "white";
        initialTimeError.innerText = "";
        finishTime.style.backgroundColor = "white";
        finishTimeError.innerText = "";
        place.style.backgroundColor = "white";
        placeError.innerText = "";
        newActivityModal.style.display = 'block';
    });

    closeActivityModal.addEventListener('click', () => {
        // Here we close the modal
        newActivityModal.style.display = 'none';
    })
}