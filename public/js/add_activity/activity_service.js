import * as packageService from "../add_package/package_service.js"
import * as packageList from "../package_list/package_list.js"
import * as activityList from "../activity_list/activity_list.js"
import * as totalEarnings from "../total_earnings/total_earnings.js"

let nextActivityId = 5;

function addActivity(activity) {
    let activePackage = packageService.getActivePkg();
    if (activePackage == null) {
        alert("You must select a package");
        return;
    }

    if(activity.finishTime < activity.initialTime) {
        alert("Finish time must be later than initial time");
        return;
    }

    let crossActivities = activePackage.activities.filter((a) => 
        (a.initialTime <= activity.initialTime && a.finishTime >= activity.initialTime && a.date == activity.date)
        || (a.initialTime <= activity.finishTime && a.finishTime >= activity.finishTime && a.date == activity.date))
    if (crossActivities.length != 0)
        alert("There would be a crossing of activities with " + crossActivities[0].title + " activity");
    else {
        totalEarnings.addAvailableYear(new Date(activity.date).getFullYear());
        activePackage.activities.push(activity);
        nextActivityId++;
        document.getElementById("addNewPerson").disabled = false;
        totalEarnings.getTotalEarnings();
        activityList.render();
        packageList.render();
    }
}

function getActivityId(){
    return nextActivityId;
}

export { 
    addActivity,
    getActivityId,
};