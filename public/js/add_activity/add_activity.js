import { Activity } from "../Models/activity.js";
import * as activityService from "./activity_service.js"

const title = document.getElementById("fActivityTitle");
const type = document.getElementById("fActivityType");
const price = document.getElementById("fActivityPrice");
const initialTime = document.getElementById("fActivityInitialTime");
const finishTime = document.getElementById("fActivityFinishTime");
const place = document.getElementById("fActivityPlace");
const titleError = document.getElementById("fTitleError");
const typeError = document.getElementById("fTypeError");
const priceError = document.getElementById("fPriceError");
const initialTimeError = document.getElementById("fInitialTimeError");
const finishTimeError = document.getElementById("fFinishTimeError");
const placeError = document.getElementById("fPlaceError");
const date = document.getElementById("fActivityDate");
const addActivityButton = document.getElementById("fAddActivity");

function setValidator(element, f) {
    element.addEventListener('focusout', f);
}

function setTitleValidator() {
    setValidator(title, () => {
        const re = /^[a-zA-Z0-9\s]+$/;
        if (re.exec(title.value) == null) {
            title.style.backgroundColor = "rgba(255,0,0,0.5)";
            titleError.innerText = "The activity title should only include letters, numbers and spaces";
        }
        else {
            title.style.backgroundColor = "white";
            titleError.innerText = "";
        }
    });
}

function setTypeValidator() {
    setValidator(type, () => {
        if (type.value != "Trek" && type.value != "Event" && type.value != "Concert" && type.value != "Hotel") {
            type.style.backgroundColor = "rgba(255,0,0,0.5)";
            typeError.innerText = "The activity type should match with options displayed on select";
        }
        else {
            type.style.backgroundColor = "white";
            typeError.innerText = "";
        }
    });
}

function setPriceValidator() {
    setValidator(price, () => {
        const re = /^[0-9]+\.[0-9]+$/;
        if (re.exec(price.value) == null) {
            price.style.backgroundColor = "rgba(255,0,0,0.5)";
            priceError.innerText = "The activity price should only include positive numbers and must have at least one float number";
        }
        else {
            price.style.backgroundColor = "white";
            priceError.innerText = "";
        }
    });
}

function setInitialTimeValidator() {
    setValidator(initialTime, () => {
        const re = /^([0-2]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
        if (re.exec(initialTime.value) == null) {
            initialTime.style.backgroundColor = "rgba(255,0,0,0.5)";
            initialTimeError.innerText = "The activity initial time should follow next format HH:MM";
        }
        else {
            initialTime.style.backgroundColor = "white";
            initialTimeError.innerText = "";
        }
    });
}

function setFinishTimeValidator() {
    setValidator(finishTime, () => {
        const re = /^([0-2]?[0-9]|2[0-4]):([0-5][0-9])(:[0-5][0-9])?$/;
        if (re.exec(finishTime.value) == null) {
            finishTime.style.backgroundColor = "rgba(255,0,0,0.5)";
            finishTimeError.innerText = "The activity finish time should follow next format HH:MM";
        }
        else {
            finishTime.style.backgroundColor = "white";
            finishTimeError.innerText = "";
        }
    });
}

function setPlaceValidator() {
    setValidator(place, () => {
        const re = /^[a-zA-Z0-9\s]+$/;
        if (re.exec(place.value) == null) {
            place.style.backgroundColor = "rgba(255,0,0,0.5)";
            placeError.innerText = "The activity place should only include letters and spaces";
        }
        else {
            place.style.backgroundColor = "white";
            placeError.innerText = "";
        }
    });
}

function setButtonListener(){
    addActivityButton.addEventListener('click', () => {

        if (title.value != '' && type.value != '' && price.value != ''
            && initialTime.value != '' && finishTime.value && date.value != ''
            && titleError.innerText == '' && typeError.innerText == '' && priceError.innerText == ''
            && initialTimeError.innerText == '' && finishTimeError.innerText == '') {
                activityService.addActivity( new Activity(
                    activityService.getActivityId(),
                    title.value,
                    type.value,
                    price.value,
                    initialTime.value,
                    finishTime.value,
                    place.value,
                    date.value
                ));
                title.value = '';
                price.value = '';
                initialTime.value = '';
                finishTime.value = '';
                date.value = '';
                place.value = '';
            }
        else {
            alert("Please fill all form fields considering its error messages");
        }
    });
}

export {
    setTitleValidator,
    setTypeValidator,
    setPriceValidator,
    setInitialTimeValidator,
    setFinishTimeValidator,
    setPlaceValidator,
    setButtonListener
}

