import * as personService from '../add_person/person_service.js'
import { Person } from '../models/person.js';

const editPersonBtn = document.getElementById('fEditPerson');
const personName = document.getElementById('fPersonName');
const personLastName = document.getElementById('fPersonLastName');
const nameError = document.getElementById('fPersonNameError');
const lastNameError = document.getElementById('fPersonLastNameError');
const personModal = document.getElementById('newPersonModal');

export function setEditListener() {
    editPersonBtn.addEventListener('click', () => {
        if (personName.value != '' && nameError.innerText == ''
        && personLastName.value != '' && lastNameError.innerText == '') {
                let isSuccessEdit = personService.editPerson(personName.value, personLastName.value);
                if(isSuccessEdit) {
                    personName.value = '';
                    personLastName.value = '';
                    personModal.style.display = 'none';
                }
            }
        else {
            alert("Please fill all form fields and consider the error messages.");
        }
    });
}