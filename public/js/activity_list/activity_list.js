import * as packageService from "../add_package/package_service.js";

const tBody = document.createElement('tbody');
const activityTable = document.getElementById('activityTable');

export function render() 
{
    let pkg = packageService.getActivePkg();
    if (pkg != null) {
        pkg.activities.sort((x, y) => (x.date < y.date) ? -1 : (x.date > y.date) ? 1 : 0 || x.initialTime.localeCompare(y.initialTime));
    }

    while (tBody.hasChildNodes()) {
        tBody.removeChild(tBody.lastChild);
    }
    while (activityTable.hasChildNodes()) {
        activityTable.removeChild(activityTable.lastChild);
    }
    if (pkg == null || pkg.activities.length == 0) {
        return;
    }
    
    renderHeader();
    activityTable.appendChild(tBody);
    renderRows(pkg.activities);
}

function renderHeader()
{
    let headerTitles = ['Title', 'Place', 'Price', 'Date', 'Initial time', 'Finish time'];
    const tHead = document.createElement('thead');
    const header = document.createElement('tr');
    for (let index = 0; index < headerTitles.length; index++) {
        const cell = document.createElement('td');
        cell.style.fontSize = "medium";
        cell.innerText = headerTitles[index];
        header.appendChild(cell);
    }
    tHead.appendChild(header);
    activityTable.appendChild(tHead);
}

function renderRows(activities)
{
    activities.forEach(activity => {
        const row = document.createElement('tr');
        const rowValues = [
            activity.title,
            activity.place,
            parseFloat(activity.price).toFixed(2),
            activity.date,
            activity.initialTime,
            activity.finishTime
        ];
        for (let index = 0; index < rowValues.length; index++) {
            const cell = document.createElement('td');
            cell.innerText = rowValues[index];
            row.appendChild(cell);
        }
        tBody.appendChild(row);
    });
}