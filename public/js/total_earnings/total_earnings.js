import * as packageService from "../add_package/package_service.js";
import * as DB from "../add_package/packageDB.js";

const yearSelect = document.getElementById('eYearSelect');
const monthSelect = document.getElementById('eMonthSelect');
const weekSelect = document.getElementById('eWeekSelect');
const total = document.getElementById('eTotalLabel');

function setUpEarningBox() {
    setUpSelects();
    setUpEarningListeners();
    getTotalEarnings();
}

function setUpSelects() {
    updateYearSelect();
    fillSelect(DB.monthNames, monthSelect);
    fillSelect(DB.weeks, weekSelect);
}

function updateYearSelect(){
    while (yearSelect.hasChildNodes()) {
        yearSelect.removeChild(yearSelect.lastChild);
    }

    DB.availableYears.sort((a, b) => b - a);
    fillYearSelect(DB.availableYears, yearSelect);
}

function fillYearSelect(list, select) {
    list.forEach((element) => {
        let opt = document.createElement('option');
        opt.value = element;
        opt.innerText = element;
        select.appendChild(opt);
    });
}

function fillSelect(list, select) {
    list.forEach((element) => {
        let opt = document.createElement('option');
        opt.value = element.value;
        opt.innerText = element.name;
        select.appendChild(opt);
    });
}

function addAvailableYear(year) {
    if (!DB.availableYears.includes(year)){
        DB.availableYears.push(year);
        updateYearSelect();
    }
}

function setUpEarningListeners() {
    yearSelect.addEventListener('change', () => {
        getTotalEarnings();
    });

    monthSelect.addEventListener('change', () => {
        updateWeekSelectState();
        getTotalEarnings();
    });

    weekSelect.addEventListener('change', () => {
        getTotalEarnings();
    });
}

function getTotalEarnings() {
    const pkgs = packageService.getPackages().filter(setFilter());
    let totalEarnings = 0;
    if (pkgs.length > 1) totalEarnings = pkgs.reduce((a, b) => parseFloat(a.profit) + parseFloat(b.profit)).toFixed(2);
    if (pkgs.length == 1) totalEarnings = parseFloat(pkgs[0].profit).toFixed(2);
    if (weekSelect.value != 0) total.innerText = "Total weekly profit is: " + totalEarnings;
    else if (monthSelect.value != 0) total.innerText = "Total monthly profit is: " + totalEarnings;
    else total.innerText = "Total annual profit is: " + totalEarnings;
}

function setFilter(){
    let year = yearSelect.value;
    let month = monthSelect.value;
    let finalDay = weekSelect.value;
    if (finalDay != 0) {
        return (p) => {
            let flag = false;
            p.activities.forEach((a) => {
                var date = new Date(a.date);
                let initialDay = (finalDay != 31) ? finalDay - 7 : finalDay - 10;
                if (date.getFullYear() ==  year && date.getMonth() + 1 == month && date.getDate() + 1 > initialDay && date.getDate() + 1 <= finalDay) 
                    flag = true;
            });
            if (flag) return p;
        }
    }
    else {
        if (monthSelect.value != 0) {
            return (p) => {
                let flag = false;
                p.activities.forEach((a) => {
                    let date = new Date(a.date);
                    if (date.getFullYear() ==  year && date.getMonth() + 1 == month) flag = true;
                });
                if (flag) return p;
            }
        }
        else {
            return (p) => {
                let flag = false;
                p.activities.forEach((a) => {
                    if (new Date(a.date).getFullYear() ==  year) flag = true;
                });
                if (flag) return p;
            }
        }
    }
}

function updateWeekSelectState(){
    if (monthSelect.value != 0)
        weekSelect.disabled = false;
    else {
        weekSelect.disabled = true;
        weekSelect.value = weekSelect.options[0].value;
    }
    
}

export { 
    setUpEarningBox,
    addAvailableYear,
    getTotalEarnings
  }