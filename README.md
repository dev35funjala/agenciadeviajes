# AgenciaDeViajes


## Team agreements

- All pull reaquests must be approved by one member of the team.
- Commits should be autodescriptive and wirtten on english.
- All pull request must have a reference to trello board.
- Branch naming convention is: {feature/hotfix/bugfix}/{trello-card}-{title}

## Wireframe

[Mockup](https://www.figma.com/file/1FhHxWACU0BCuTrzLdQbvg/Agencia-de-Viajes-Mockup?node-id=0%3A1)

## Trello board

[Trello Board](https://trello.com/b/zNDoY2SS/agenciadeviajes)

## Workflow

In this project we'll gonna use Github Flow

## Definition of done (DoD)

- Story must be uploaded for review by making a make request.
- Story must be approved by one of the other developers in the team.
- Story is merge into main.

## Definition of ready (DoR)

- Story is well defined with clear acceptance criteria
- Story has a deadline.
- Story has assigned it's story points.
- Has all prerequisits defined and already merged on main.

