import * as addPackage from "./add_package.js"

const newPackageBtn = document.getElementById('addNewPackage');
const newPackageModal = document.getElementById('newPackageModal');
const closeAddPackage = document.getElementById('closeAddPackage');

export function packageModalCfg(){
    addPackage.setTitleValidator();
    addPackage.setCityValidator();
    addPackage.setSpotsValidator();
    addPackage.setAddPackageCallback();

    newPackageBtn.addEventListener('click', () => {
        // Here we open the model
        const packageTitle = document.getElementById('fPackageTitle');
        const packageCity = document.getElementById('fpackageCity');
        const packageSpotsAvailable = document.getElementById('fpackageSpotsAvailable');
        const titleError = document.getElementById('fTitlePackageError');
        const cityError = document.getElementById('fCityPackageError');
        const spotsError = document.getElementById('fSpotsPackageError');
        packageTitle.style.backgroundColor = 'white';
        titleError.innerText = "";
        packageCity.style.backgroundColor = 'white';
        cityError.innerText = "";
        packageSpotsAvailable.style.backgroundColor = 'white';
        spotsError.innerText = "";
        newPackageModal.style.display = 'block';
    });

    closeAddPackage.addEventListener('click', () => {
        // Here we close the modal
        newPackageModal.style.display = 'none';
    })
}