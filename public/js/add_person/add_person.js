import { Person } from "../models/person.js";
import * as personService from "./person_service.js";

const personName = document.getElementById('fPersonName');
const personLastName = document.getElementById('fPersonLastName');
const addPerson = document.getElementById('fAddPerson');
const nameError = document.getElementById('fPersonNameError');
const lastNameError = document.getElementById('fPersonLastNameError');
const makeReservatonBtn = document.getElementById('fReservePackage');

function setValidator(element, f)
{
    element.addEventListener('focusout', f);
}
function setNameValidator()
{
    setValidator(personName, () => {
        const re = /^[a-zA-Z\s]+$/;
        if (re.exec(personName.value) == null) {
            personName.style.backgroundColor = "rgba(255,0,0,0.5)";
            nameError.innerText = "The name should only include letters and spaces.";
        } else {
            personName.style.backgroundColor = 'white';
            nameError.innerText = "";
        }
    });
}
function setLastNameValidator()
{
    setValidator(personLastName, () => {
        const re = /^[a-zA-Z\s]+$/;
        if (re.exec(personLastName.value) == null) {
            personLastName.style.backgroundColor = "rgba(255,0,0,0.5)";
            lastNameError.innerText = "The name should only include letters and spaces.";
        } else {
            personLastName.style.backgroundColor = 'white';
            lastNameError.innerText = "";
        }
    });
}

function setAddPersonCallback() {
    addPersonToPkg(addPerson, personService.addPerson)
}

function setReservationCallback() {
    addPersonToPkg(makeReservatonBtn, personService.makeReservation)
}

function addPersonToPkg(element , fun) {
    element.addEventListener('click', () => {
        if (personName.value != '' && nameError.innerText == ''
        && personLastName.value != ''&& lastNameError.innerText == '') {
                fun(new Person(
                    personService.getPersonId(),
                    personName.value,
                    personLastName.value,
                ));
                personName.value = '';
                personLastName.value = '';
            }
        else {
            alert("Please fill all form fields and consider the error messages.");
        }
    });
}

export
{
    setNameValidator,
    setLastNameValidator,
    setAddPersonCallback,
    setReservationCallback,
}