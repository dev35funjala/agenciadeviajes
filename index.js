import * as newPackage from "./public/js/add_package/package_modal.js";
import * as newActivity from "./public/js/add_activity/activity_modal.js";
import * as newPerson from "./public/js/add_person/person_modal.js";
import * as packageList from "./public/js/package_list/package_list.js";
import * as personList from "./public/js/person_list/person_list.js";
import * as activityList from "./public/js/activity_list/activity_list.js";
import * as packageFilter from "./public/js/package_filter/package_filter.js";
import * as totalEarnings from "./public/js/total_earnings/total_earnings.js";
import * as personFilter from "./public/js/person_filter/person_filter.js";

newPackage.packageModalCfg();
newPerson.personModalCfg();
newActivity.activityModalCfg();
packageList.render();
personList.render();
activityList.render();
packageFilter.setup();
personFilter.setup();
totalEarnings.setUpEarningBox();
