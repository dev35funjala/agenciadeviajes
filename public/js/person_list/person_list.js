import * as personService from '../add_person/person_service.js'
import * as personModal from '../add_person/person_modal.js'
import * as packageService from '../add_package/package_service.js'

const tBody = document.createElement('tbody');
const peopleTable = document.getElementById('peopleTable');

export function render() {
    const pkg = packageService.getActivePkg();
    const people = applyFilter(pkg);
    while (tBody.hasChildNodes()) {
        tBody.removeChild(tBody.lastChild);
    }
    while (peopleTable.hasChildNodes()) {
        peopleTable.removeChild(peopleTable.lastChild);
    }
    if(pkg == null || people.length == 0)
    {
        return;
    }
    
    renderHeader();
    peopleTable.appendChild(tBody);
    renderRows(people);
}

function renderHeader()
{
    let headerTitles = ['Name', 'Last Name', 'Actions'];
    const tHead = document.createElement('thead');
    const header = document.createElement('tr');
    for (let index = 0; index < headerTitles.length; index++) {
        const cell = document.createElement('td');
        cell.style.fontSize = "medium";
        cell.innerText = headerTitles[index];
        header.appendChild(cell);
    }
    tHead.appendChild(header);
    peopleTable.appendChild(tHead);
}

function renderRows(people) {
    people.forEach(person => {
        const row = document.createElement('tr');
        const rowValues = [
            person.name,
            person.lastName,
        ];
        for (let index = 0; index < rowValues.length; index++) {
            const cell = document.createElement('td');
            cell.innerText = rowValues[index];
            row.appendChild(cell);
        }
        row.appendChild(getActionButtons(person));
        tBody.appendChild(row);
    });
}

function getActionButtons(person) {
    const activePkg = packageService.getActivePkg();
    const buttons = document.createElement('td');
    if (activePkg.reservations.includes(person)) 
    {
        const trashIcon = document.createElement('i');
        trashIcon.className = 'fa-solid fa-trash-can';
        buttons.appendChild(trashIcon);
        trashIcon.addEventListener('click', () => {
            personService.cancelReservation(person);
        });
    }
    const pencilIcon = document.createElement('i');
    pencilIcon.className = 'fa-solid fa-pencil';
    buttons.appendChild(pencilIcon);
    pencilIcon.addEventListener('click', () => {
        personModal.startModal(false, person);
        personService.setActivePerson(person);
    });
    return buttons;
}

function applyFilter(pkg){
    if(pkg == null){
        return [];
    }
    
    const filterValue = document.getElementById('personFilterValue').value;
    const filterOption = document.getElementById('personFilterOption').value;
    let list;

    switch(filterOption){
        case "None":
            return pkg.people;
        case "Name":
            list = pkg.people.filter((a) => a.name.includes(filterValue));
            break;
        case "Last Name":
            list = pkg.people.filter((a) => a.lastName.includes(filterValue));
            break;
    }

    if(list.length == 0){
        alert("No matching people!");
        return pkg.people;
    }
    return list;
}