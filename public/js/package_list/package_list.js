import * as packageService from "../add_package/package_service.js";
import * as editPackage from "../edit_package/edit_package.js";
import * as pkgFilter from "../package_filter/package_filter.js"

const tBody = document.createElement('tbody');
const packageTable = document.getElementById('packagesTable');
const pkgFilterOption = document.getElementById('pkgFilterOption');
let filterApplied = false;

function render() {
    const packages = applyFilter();
    while (tBody.hasChildNodes()) {
        tBody.removeChild(tBody.lastChild);
    }
    while (packageTable.hasChildNodes()) {
        packageTable.removeChild(packageTable.lastChild);
    }
    if (packages.length == 0){
        return; // Si no hay elementos no renderizo la tabla
    }
    renderHeader();
    packageTable.appendChild(tBody);
    renderRows(packages);
}

function renderHeader() {
    let headerTitles = ['Name', 'City', 'Price', 'Profit', 'Actions'];
    const tHead = document.createElement('thead');
    const header = document.createElement('tr');
    for (let index = 0; index < headerTitles.length; index++) {
        const cell = document.createElement('td');
        cell.innerText = headerTitles[index];
        cell.style.fontSize = "medium";
        header.appendChild(cell);
    }
    tHead.appendChild(header);
    packageTable.appendChild(tHead);
} 

function renderRows(packages) {
    const activePkg = packageService.getActivePkg();
    packages.forEach(pkg => {
        const row = document.createElement('tr');
        const rowValues = [
            pkg.title,
            pkg.city,
            pkg.price,
            pkg.profit
        ];
        for (let index = 0; index < rowValues.length; index++) {
            const cell = document.createElement('td');
            cell.innerText = rowValues[index];
            cell.addEventListener('click', () => {
                packageService.setActivePkg(pkg);
            });
            row.appendChild(cell);
        }
        row.style.cursor = "pointer";
        row.appendChild(getActionButtons(pkg));
        if (activePkg == pkg) {
            row.style.backgroundColor = '#FDDA0D';
        }
        tBody.appendChild(row);
    });
}

function getActionButtons(pkg) {
    const buttons = document.createElement('td');
    const trashIcon = document.createElement('i');
    trashIcon.className = 'fa-solid fa-trash-can';
    const pencilIcon = document.createElement('i');
    pencilIcon.className = 'fa-solid fa-pencil';
    buttons.appendChild(trashIcon);
    buttons.appendChild(pencilIcon);
    trashIcon.addEventListener('click', () => {
        pkgFilter.setToNone();
        packageService.deletePackage(pkg);
        render();
    });
    pencilIcon.addEventListener('click', () => {
        packageService.setActivePkg(pkg)
        editPackage.setup();
    });
    return buttons;
}

function applyFilter() {
    const pkgs = packageService.getPackages();
    if (filterApplied == false) {
        return pkgs;
    }
    const pkgFilterValue = document.getElementById('pkgFilterValue').value;
    let list;
    switch (pkgFilterOption.value) {
        case "None":
            return pkgs;
        case "City":
            list = pkgs.filter((p) => p.city.includes(pkgFilterValue));
            break;
        case "Price":
            list = pkgs.filter((p) => p.price <= parseFloat(pkgFilterValue));
            break;
        case "Date":
            list = pkgs.filter((p) => p.startDate <= Date.parse(pkgFilterValue));
            break;
    }
    if (list.length == 0) {
        alert("No matching tour package!");
        return pkgs;
    }
    return list;
}

function setFilterApplied() {
    filterApplied = true;
}

function clearFilterApplied() {
    filterApplied = false;
}

export {
    render,
    setFilterApplied,
    clearFilterApplied,
}