export class Activity {
    constructor(id, title, type, price, initialTime, finishTime, place, date) {
        this.id = id;
        this.title = title;
        this.type = type;
        this.price = price;
        this.initialTime = initialTime;
        this.finishTime = finishTime;
        this.place = place;
        this.date = date;
    }
}