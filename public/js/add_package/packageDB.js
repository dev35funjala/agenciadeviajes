import { Package } from "../models/package.js";
import { Activity } from "../models/activity.js";
import { Person } from "../models/person.js";

export let packages = [
    new Package(
        0,
        "Tierra de aventuras",
        "Uyuni, Bolivia",
        20,
        {
            activities: [
                new Activity(
                    0,
                    "Visita Cementerio de trenes",
                    "Event",
                    "12.2",
                    "09:00",
                    "10:30",
                    "Cementerio de trenes",
                    "2022-06-06"
                ),
                new Activity(
                    1,
                    "Paseo por el Salar",
                    "Event",
                    "80.2",
                    "10:45",
                    "18:30",
                    "Salar de Uyuni",
                    "2022-06-06"
                ),
            ],
            registered: [
                new Person(0, "Raul", "Camacho"),
                new Person(1, "Carlos", "Zurita"),
                new Person(2, "Michael", "Mollinedo"),
            ],
            reservations: [
                new Person(3, "Michael", "Jackson"),
                new Person(4, "Michael", "Jordan"),
                new Person(5, "Jhonny", "Depp"),
            ]
        }
    ),
    new Package(
        1,
        "Cancun Whale Shark Tours",
        "Cancun, Mexico",
        15,
        {
            activities: [
                new Activity(
                    2,
                    "Aventura en parasailing",
                    "Event",
                    "25.5",
                    "09:00",
                    "10:30",
                    "Playa Cancun",
                    "2021-08-06"
                ),
                new Activity(
                    3,
                    "Crucero en trimarán a Isla Mujeres",
                    "Event",
                    "85.0",
                    "12:00",
                    "20:30",
                    "trimarán",
                    "2021-08-06"
                ),
                new Activity(
                    4,
                    "Aventura de buceo de superficie",
                    "Event",
                    "80.0",
                    "10:00",
                    "17:00",
                    "Tulum",
                    "2021-08-07"
                ),
            ],
            registered: [
                new Person(6, "Sofia", "Zapata"),
                new Person(7, "Luis", "Arce"),
                new Person(8, "Jose", "Campero"),
            ],
            reservations: [
                new Person(9, "Luis", "Mendez"),
                new Person(10, "Lucia", "Cortez"),
                new Person(11, "Juan", "Perez"),
            ]
        }
    ),
];

export let availableYears = [2022, 2021];
export const monthNames = [
    {"name" : "-", 'value' : 0}, 
    {"name" : "January", 'value': 1},
    {"name" : "February", 'value': 2},
    {"name" : "March", 'value': 3},
    {"name" : "April", 'value': 4},
    {"name" : "May", 'value': 5},
    {"name" : "June", 'value': 6},
    {"name" : "July", 'value': 7},
    {"name" : "August", 'value': 8},
    {"name" : "September", 'value': 9},
    {"name" : "October", 'value': 10},
    {"name" : "November", 'value': 11},
    {"name" : "December", 'value': 12}
];
export const weeks = [
    {"name" : "-", "value" : 0},
    {"name" : "1st week", "value" : 7},
    {"name" : "2nd week", "value" : 14},
    {"name" : "3rd week", "value" : 21},
    {"name" : "4th week", "value" : 31}
];

export function remove(pkg) {
    packages = packages.filter((p) => {
        return p.id != pkg.id;
    });
}