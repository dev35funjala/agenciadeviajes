import * as addPerson from "./add_person.js"
import * as editPerson from '../edit_person/edit_person.js'

const newPersonBtn = document.getElementById('addNewPerson');
const newPersonModal = document.getElementById('newPersonModal');
const closeAddPerson = document.getElementById('closeAddPerson');
const addPersonBtn = document.getElementById('fAddPerson');
const makeReservatonBtn = document.getElementById('fReservePackage');
const editPersonBtn = document.getElementById('fEditPerson');
const modalTitle = document.getElementById('fModalTitle');


export function personModalCfg(){
    addPerson.setNameValidator();
    addPerson.setLastNameValidator();
    addPerson.setAddPersonCallback();
    addPerson.setReservationCallback();
    editPerson.setEditListener();
    
    newPersonBtn.addEventListener('click', () => startModal(true));

    closeAddPerson.addEventListener('click', () => {
        // Here we close the modal
        newPersonModal.style.display = 'none';
    })
}

export function startModal(isCreation, person){
    let personName = document.getElementById('fPersonName');
    let personLastName = document.getElementById('fPersonLastName');
    const nameError = document.getElementById('fPersonNameError');
    const lastNameError = document.getElementById('fPersonLastNameError');
    personName.style.backgroundColor = 'white';
    nameError.innerText = "";
    personLastName.style.backgroundColor = 'white';
    lastNameError.innerText = "";
    newPersonModal.style.display = 'block';
    if (isCreation){
        modalTitle.innerHTML = "ADD NEW CUSTOMER";
        makeReservatonBtn.style.display = 'block';
        addPersonBtn.style.display = 'block';
        editPersonBtn.style.display = 'none';
    }
    else {
        modalTitle.innerHTML = "EDIT A CUSTOMER";
        editPersonBtn.style.display = 'block';
        makeReservatonBtn.style.display = 'none';
        addPersonBtn.style.display = 'none';
        personName.value = person.name;
        personLastName.value = person.lastName;
    }
}