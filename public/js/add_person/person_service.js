import * as packageService from "../add_package/package_service.js";
import * as packageList from "../package_list/package_list.js";
import * as peopleList from "../person_list/person_list.js";
import * as totalEarnings from "../total_earnings/total_earnings.js"

let nextPersonId = 12;
let activePerson = null;

function addPerson(person) {
    addPersonToPkg(person, 'registered');
}

function makeReservation(person) {
    addPersonToPkg(person, 'reservations');
}

function addPersonToPkg(person, listName) {
    const pkg = packageService.getActivePkg();
    if (pkg == null) {
        nextPersonId--;
        alert("No package selected");
        return;
    }

    let destinationList;
    if (listName == 'reservations') {
        destinationList = pkg.reservations;
    } else {
        destinationList = pkg.registered;
    }

    if (!pkg.hasSpotsAvailable()) {
        nextPersonId--;
        alert("Package is already full!");
        return;
    }
    destinationList.push(person);
    totalEarnings.getTotalEarnings();
    peopleList.render();
    packageList.render();
}

function cancelReservation(person)
{
    const activePkg = packageService.getActivePkg();
    activePkg.reservations = activePkg.reservations.filter((p) => p != person);
    peopleList.render();
    packageList.render();
}

function getPersonId() {
    return nextPersonId++;
}

function setActivePerson(person){
    activePerson = person;
}

function editPerson(name, lastName) {
    activePerson.name = name;
    activePerson.lastName = lastName;
    peopleList.render();
    return true;
}


export {
    addPerson,
    getPersonId,
    makeReservation,
    cancelReservation,
    setActivePerson,
    editPerson
};