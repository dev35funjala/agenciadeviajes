import * as packageList from "../package_list/package_list.js";

const applyFilterBtn = document.getElementById('pkgApplyFilter');
const pkgFilterOption = document.getElementById('pkgFilterOption');

export function setup() {
    setupInput();
    setupBtn();
}

function setupInput() {
    pkgFilterOption.addEventListener('change', () => setupFilter(pkgFilterOption.value));
}

function setupBtn() {
    applyFilterBtn.addEventListener("click", () => {
        packageList.setFilterApplied();
        packageList.render();
    });
}

function setupFilter(option) {
    const pkgFilterValue = document.getElementById('pkgFilterValue');
    if (option != 'None') {
        applyFilterBtn.disabled = false;
    } else {
        applyFilterBtn.disabled = true;
    }
    const input = document.createElement('input');
    input.id = "pkgFilterValue";
    switch (option) {
        case "None":
            input.type = "text";
            input.disabled = true;
            packageList.clearFilterApplied();
            packageList.render();
            break;
        case "City":
            input.type = "text";
            break;
        case "Price":
            input.type = "number";
            break;
        case "Date":
            input.type = "date";
            break;
    }
    pkgFilterValue.parentNode.replaceChild(input, pkgFilterValue);
}

export function setToNone() {
    pkgFilterOption.value = 'None';
    setupFilter('None');
}