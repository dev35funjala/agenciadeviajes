import * as peopleList from "../person_list/person_list.js";
import * as activityList from "../activity_list/activity_list.js";
import * as packageList from "../package_list/package_list.js";
import {packages} from "./packageDB.js";
import * as DB from "./packageDB.js";
import * as personFilter from "../person_filter/person_filter.js"

const newPersonButton = document.getElementById("addNewPerson");
const newActivityBtn = document.getElementById("newActivityBtn");

let nextPackageId = packages.length;
let activePkg = packages[0];

function addPackage(touristPackage) {
    packages.push(touristPackage);
    setActivePkg(touristPackage);
}

function deletePackage(touristPackage) {
    if (touristPackage == activePkg) {
        setActivePkg(null);
    }
    DB.remove(touristPackage);
}

function editPackage(newTitle, newCity, newSpots) {
    activePkg.title = newTitle;
    activePkg.city = newCity;
    activePkg.spotsAvailable = newSpots;
}

function getPackages() {
    return packages;
}

function getPackageId() {
    return nextPackageId++;
}

function setActivePkg(pkg) {
    personFilter.setToNone();
    activePkg = pkg;
    if (activePkg == null) {
        newPersonButton.disabled = true;
        newActivityBtn.disabled = true;
    } else if (activePkg.activities.length > 0){
        newActivityBtn.disabled = false;
        newPersonButton.disabled = false;
    } else {
        newActivityBtn.disabled = false;
        newPersonButton.disabled = true;
    }
    packageList.render();
    peopleList.render();
    activityList.render();
}

function getActivePkg() {
    return activePkg;
}

export { 
    addPackage,
    deletePackage,
    editPackage,
    getPackages,
    getPackageId,
    setActivePkg,
    getActivePkg,
};